﻿// Creates a new XMLHttpRequest object to provides the API functionality to allow us to transfer data to and from the server. 
var http = new XMLHttpRequest();
// Set the server reference part of URL that does not change.
var coreUrl = "http://localhost:65026/api/";
// Set the request URL for business unit requests.
var buUrl = coreUrl + "BusinessUnit";
// Set the request URL for getting a list of active staff by business unit.
var staffListUrl = coreUrl + "Staff?code=";
//  Set the request URL for getting a member of staffs details by staff code.
var staffDetailUrl = coreUrl+ "Staff/";

// Function called when the HTML page is loaded into the browser window & to send a HTTP request to GET a list of business units.
function start() {
    // Invoke the hideStaffList function so the list of staff members is not shown on the start up page.
    hideStaffList();
    // Sets the readystate object that will be called each time the XMLHttpRequest object readystate status changes.
    http.onreadystatechange = getBuList;
    // Specify the criteria (HTTP request type & URL) of the request.
    http.open("GET", buUrl);
    // Send the request.
    http.send();
}

// Function called to get a list of business units from the HTTP reply.
function getBuList() {
    // Check the XMLHttpRequest readystate to see if the request is finished and response is ready, and that the status is OK.
    if (http.readyState == 4 && http.status == 200) {
        // Unpack the HTTP response text and get the JSON array object.
        var buisinessUnits = JSON.parse(http.responseText);
        // If the response is not empty.
        if (buisinessUnits != null) {
            // Invoke the displayItemsInBuList function and pass in the received JSON array object.
            displayItemsInBuList(buisinessUnits);
        // If the response is empty however.
        } else {
            // Invoke the hideAll function.
            hideAll();
        }
    }
}

// Function called to display the received business units in a table.
function displayItemsInBuList(arr) {
    // Get the table object from the HTML document.
    var table = document.getElementById("buList");
    // Empty the table tag of it's current contents.
    table.innerHTML = "";
    // Run a for loop to create a table to dsiplay each business unit.
    for (var i = 0; i < arr.length; i++) {
        // Create a new row for each business unit.
        var row = table.insertRow(0);
        // Create two cells on each row.
        var cell1 = row.insertCell(0);     
        var cell2 = row.insertCell(1);
        // Fill cell 1 with the business unit title.
        cell1.innerHTML = arr[i].title;
        // Give cell 1 an id using the business code as the tag.
        var id = arr[i].businessUnitCode;
        // Bind that tag to the "list staff" anchor tag placed in cell 2 of the row.
        cell2.innerHTML = "&nbsp&nbsp&nbsp&nbsp<a href='#'     id='" + id + "' " + " >List Staff</a>";
        // When one of the "List Staff" links is pressed, get the element id (business unit code!) and invoke the getStaff function, passing the id to it.
        document.getElementById(id).onclick = getStaff;
    }
}

// Function called to send a HTTP request to GET a list of staff.
function getStaff(e) {
    // Add the business unit code to the modified url for the GET request.
    var detailUrl = staffListUrl + e.target.id;
    // Sets the new readystate object that will be called each time the XMLHttpRequest object readystate status changes.
    http.onreadystatechange = requestDetail;
    // Specify the new criteria (HTTP request type & URL) of the request.
    http.open("GET", detailUrl);
    // Send the request.
    http.send();
}

// Function called to get the list of staff members from the HTTP reply.
function requestDetail() {
    // Check the XMLHttpRequest readystate to see if the request is finished and response is ready, and that the status is OK.
    if (http.readyState == 4 && http.status == 200) {
        // Unpack the HTTP response text and get the JSON array object.
        var staffList = JSON.parse(http.responseText);
        // If the response is not empty.
        if (staffList != null) {
            // Invoke the displayStaffList function and pass in the received JSON array object.
            displayStaffList(staffList);
        // If the response is empty however.
        } else {
            // Invoke the hideStaffList function.
            hideStaffList();
        }
    }
}

// Function called to display the received staff list in a table.
function displayStaffList(arr) {
    // Set the HTML element with id "staffListHeader" as visible on the webpage.
    document.getElementById("staffListHeader").style.visibility = "visible";
    // Set the HTML element with id "Staffdetail" as invisible.
    document.getElementById("Staffdetail").style.visibility = "hidden";
    // Get the table object from the HTML document.
    var table = document.getElementById("staffList");
    // Set the HTML element with id "staffList" as visible on the webpage.
    table.style.visibility = "visible";
    // If the received JSON object is not null/empty.
    if (arr != null)
    {
        // Empty the table tag of it's current contents.
        table.innerHTML = "";
        // Run a for loop to create a table to dsiplay each member of staff.
        for (var i = 0; i < arr.length; i++) {
            // Create a new row for each member of staff.
            var row = table.insertRow(0);
            // Create two cells on each row.
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            // Fill cell 1 with the persons full name.
            cell1.innerHTML = arr[i].fullName;
            // Give cell 1 an id using the staff code as the tag.
            var id = arr[i].staffCode;
            // Bind that tag to the "Staff Detail" anchor tag placed in cell 2 of the row.
            cell2.innerHTML = "&nbsp&nbsp&nbsp&nbsp<a href='#'     id='" + id + "' " + " >Staff Detail</a>";
            // When one of the "Staff Detail" links is pressed, get the element id (staff code!) and invoke the getStaffDetails function, passing the id to it.
            document.getElementById(id).onclick = getStaffDetails;
        }
    }
}

// Function called to send a HTTP request to GET a staff members details.
function getStaffDetails(e) {
    // Add the staff id to the modified url for the GET request.
    var detailUrl = staffDetailUrl + e.target.id;
    // Sets the new readystate object that will be called each time the XMLHttpRequest object readystate status changes.
    http.onreadystatechange = requestStaffDetail;
    // Specify the new criteria (HTTP request type & URL) of the request.
    http.open("GET", detailUrl);
    // Send the request.
    http.send();
}

// Function called to get the staff members details from the HTTP reply.
function requestStaffDetail() {
    // Check the XMLHttpRequest readystate to see if the request is finished and response is ready, and that the status is OK.
    if (http.readyState == 4 && http.status == 200) {
        // Unpack the HTTP response text and get the JSON array object.
        var staffDetail = JSON.parse(http.responseText);
        // If the response is not empty.
        if (staffDetail != null) {
            // Invoke the displayStaffDetail function and pass in the received JSON array object.
            displayStaffDetail(staffDetail);
            // If the response is empty however.
        } else {
            // Invoke the hideStaffList function.
            hideStaffDetail();
        }
    }
}

// This function is used to display the staff details section of the web page.
function displayStaffDetail(staff) {
    // Set the HTML element with id "Staffdetail" as visible.
    document.getElementById("Staffdetail").style.visibility = "visible";
    // Create an array of the staff details element id names.
    document.getElementById("staffCode").innerHTML = staff.staffCode;
    document.getElementById("staffFullName").innerHTML = staff.firstName
    document.getElementById("staffDOB").innerHTML = staff.dob;
    document.getElementById("staffStartDate").innerHTML = staff.startDate;
    document.getElementById("staffProfile").innerHTML = staff.profile;
    document.getElementById("staffEmail").innerHTML = staff.emailAddress;
}

// This function is used to hide the business units table section of the web page.
function hideAll() {
    // Set the HTML element with id "buList" as invisible.
    document.getElementById("buList").style.visibility = "hidden";
    // Invoke the hideStaffList function.
    hideStaffList();
}

// This function was used to hide the staff list section of the web page.
function hideStaffList() {
    // Set the HTML element with id "staffListHeader" as invisible.
    document.getElementById("staffListHeader").style.visibility = "hidden";
    // Set the HTML element with id "staffList" as invisible.
    document.getElementById("staffList").style.visibility = "hidden";
    // Set the HTML element with id "Staffdetail" as invisible.
    document.getElementById("Staffdetail").style.visibility = "hidden";
            
}

// This function is used to hide the staff details section of the web page.
function hideStaffDetail() {
    // Set the HTML element with id "Staffdetail" as invisible.
    document.getElementById("Staffdetail").style.visibility = "hidden";
}

// When the HTML page is loaded into the browser window. Run the start() function.
window.onload = start;
