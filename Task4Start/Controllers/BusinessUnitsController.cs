﻿using System.Net.Http;
using System.Web.Mvc;
using System.Threading.Tasks;
using Task4Start.Models;
using SkillsCoDbfModel;
using Task4Start.Repository;
using Task4Start.SkillService;
using System;

namespace Task4Start.Controllers
{
    public class BusinessUnitsController : Controller
    {
        private IDataRepository dataRepository;
        private ISkillsRepository skillsRepository;
        private IAdminRepository adminRepository;

        /// <summary>
        /// Default Constructor.
        /// Instantiates objects for the required Repository/DAL interfaces.
        /// </summary>
        public BusinessUnitsController()
        {
            this.dataRepository = new DataRepository(new StaffSkillsModel());
            this.skillsRepository = new SkillsRepository(new SkillServiceClient());
            this.adminRepository = new AdminRepository(new HttpClient());
        }



        /// <summary>
        /// Controller action to display a list/index of available business units.
        /// </summary>
        /// <returns>A view model to display.</returns>
        // GET: BusinessUnit
        public async Task<ActionResult> Index()
        {
            try
            {
                HttpResponseMessage response = await adminRepository.RetryGetBu();

                var viewModel = await BusinessUnitVM.buildList(response);

                return View(viewModel);
            }
            catch (Exception e)
            {
                // Incase of an exception then user is directed to the last page that can be correctly displayed.
                return RedirectToAction("Index", "Home");
            }
        }



        /// <summary>
        /// Controller action to display a list of staff associated with the a business unit.
        /// </summary>
        /// <param name="id">The selected business unit.</param>
        /// <returns>A view model to display.</returns>
        // GET: BusinessUnit/BuStaff/ABC
        public async Task<ActionResult> BuStaff(string id)
        {
            try
            {
                HttpResponseMessage response = await adminRepository.RetryGetBuStaff(id);

                var viewModel = await StaffVM.buildList(response, id);

                return View(viewModel);
            }
            catch (Exception e)
            {
                // Incase of an exception then user is directed to the last page that can be correctly displayed.
                return RedirectToAction("Index");
            }
        }



        /// <summary>
        /// Controller action to display a list of skills associated with a business unit.
        /// </summary>
        /// <param name="id">The selected business unit.</param>
        /// <returns>A view model to display.</returns>
        // GET: BusinessUnit/BuSkills/ABC
        public async Task<ActionResult> BuSkills(string id)
        {
            try
            {
                HttpResponseMessage response = await adminRepository.RetryGetBuStaff(id);
                
                var skills = await skillsRepository.RetryGetSkills();

                var allStSkills = await dataRepository.GetStaffSkills();

                var viewModel = await SkillVM.buildList(skills, response, allStSkills, id);

                return View(viewModel);
            }
            catch (Exception e)
            {
                // Incase of an exception then user is directed to the last page that can be correctly displayed.
                return RedirectToAction("Index");
            }
        }

    }
}