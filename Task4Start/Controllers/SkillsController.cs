﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Task4Start.Models;
using Task4Start.Repository;
using Task4Start.SkillService;

namespace Task4Start.Controllers
{
    public class SkillsController : Controller
    {
        private ISkillsRepository skillsRepository;

        /// <summary>
        /// Default Constructor.
        /// Instantiates objects for the required Repository/DAL interfaces.
        /// </summary>
        public SkillsController()
        {
            this.skillsRepository = new SkillsRepository(new SkillServiceClient());
        }


        /// <summary>
        /// Controller action to display a list of available skills.
        /// </summary>
        /// <returns>A view model to display.</returns>
        // GET: Skills
        public async Task<ActionResult> Index()
        {
            try
            {
                var skills = await skillsRepository.RetryGetSkills();

                var viewModel = SkillVM.buildList(skills);
                
                return View(viewModel);
            }
            catch (Exception e)
            {
                // Incase of an exception then user is directed to the last page that can be correctly displayed.
                return RedirectToAction("Index", "BusinessUnits");
            }
        }

    }
}