﻿using SkillsCoDbfModel;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Task4Start.Models;
using Task4Start.Repository;
using Task4Start.SkillService;

namespace Task4Start.Controllers
{
    public class StaffsController : Controller
    {
        private IDataRepository dataRepository;
        private ISkillsRepository skillsRepository;

        /// <summary>
        /// Default Constructor.
        /// Instantiates objects for the required Repository/DAL interfaces.
        /// </summary>
        public StaffsController()
        {
            this.dataRepository = new DataRepository(new StaffSkillsModel());
            this.skillsRepository = new SkillsRepository(new SkillServiceClient());
        }



        /// <summary>
        /// Controller action to display a list of a staff members skills.
        /// </summary>
        /// <param name="id">The member of staff.</param>
        /// <returns>A view model to display.</returns>
        // GET: Staff/StaffSkills/ABC
        public async Task<ActionResult> StaffSkills(string id)
        {
            try
            {
                var stSkills = await dataRepository.GetAStaffsSkills(id);

                var viewModel = SkillVM.buildList(stSkills, id);
                
                return View(viewModel);
            }
            catch (Exception e)
            {
                // Incase of an exception then user is directed to the last page that can be correctly displayed.
                return RedirectToAction("Index", "BusinessUnits");
            }
        }



        /// <summary>
        /// Controller action to select an available skill to add to a member of staff.
        /// </summary>
        /// <param name="id">The member of staff.</param>
        /// <returns>A view model to display.</returns>
        // GET: Staff/AddSkill/ABC
        public async Task<ActionResult> AddSkill(string id)
        {
            try
            {
                var skills = await skillsRepository.RetryGetSkills();

                var stSkills = await dataRepository.GetAStaffsSkills(id);

                SkillVM viewModel = new SkillVM();

                viewModel.buildSkList(skills, stSkills, id);
                
                return View(viewModel);
            }
            catch (Exception e)
            {
                // Incase of an exception then user is directed to the last page that can be correctly displayed.
                return RedirectToAction("Index", "BusinessUnits");
            }
        }



        /// <summary>
        /// Controller action to add a skill to a member of staff.
        /// </summary>
        /// <param name="skillVM">The member of staff selected.</param>
        /// <returns>A redirect to the "StaffSkills" action.</returns>
        // POST: Staff/AddSkill/ABC
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddSkill([Bind(Include = "staffCode, selectedSkCode")] SkillVM skillVM)
        {
            try
            {
                var model = SkillVM.buildModel(skillVM);

                dataRepository.SaveModel(model);

                return RedirectToAction("StaffSkills", new { id = skillVM.staffCode });
            }
            catch (Exception e)
            {
                // Incase of an exception then user is directed to the last page that can be correctly displayed.
                return RedirectToAction("StaffSkills", new { id = skillVM.staffCode });
            }
        }

    }
}