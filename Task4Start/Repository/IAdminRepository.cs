﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Task4Start.Repository
{
    public interface IAdminRepository : IDisposable
    {
        // Available interface methods for API calls.
        Task<HttpResponseMessage> RetryGetBu();
        Task<HttpResponseMessage> RetryGetBuStaff(string code);
        Task<HttpResponseMessage> RetryGetAStaff(string code);
    }
}