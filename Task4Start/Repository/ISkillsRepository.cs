﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Task4Start.Repository
{
    public interface ISkillsRepository : IDisposable
    {
        // Available interface methods for WCF service requests.
        Task<IEnumerable<SkillService.SkillsDTO>> RetryGetSkills();
    }
}