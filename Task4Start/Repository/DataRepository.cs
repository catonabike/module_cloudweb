﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SkillsCoDbfModel;
using System.Data.Entity;

namespace Task4Start.Repository
{
    public class DataRepository : IDataRepository, IDisposable
    {
        private StaffSkillsModel db;

        /// <summary>
        /// Constructor to instantiate an object reference of the database.
        /// </summary>
        /// <param name="db">The selected database.</param>
        public DataRepository(StaffSkillsModel db)
        {
            this.db = db;
        }



        /// <summary>
        /// Asynchronous method to get a list of all skills associated with a member of staff.
        /// </summary>
        /// <param name="code">The members staffCode.</param>
        /// <returns>A list of skills.</returns>
        public async Task<List<Skill>> GetAStaffsSkills(string code)
        {
            return await db.Skills.Where(s => s.staffCode == code).ToListAsync();
        }

        /// <summary>
        /// Asynchronous method to get a list of all staff skills.
        /// </summary>
        /// <returns>A list of skills.</returns>
        public async Task<List<Skill>> GetStaffSkills()
        {
            return await db.Skills.ToListAsync();
        }

        /// <summary>
        /// A method to save an domain model to the database.
        /// </summary>
        /// <param name="model">The domain model.</param>
        public void SaveModel(Skill model)
        {
            db.Skills.Add(model);

            db.SaveChanges();
        }



        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~DataRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}