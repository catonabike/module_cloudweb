﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Task4Start.SkillService;

namespace Task4Start.Repository
{
    public class SkillsRepository : ISkillsRepository, IDisposable
    {
        private SkillServiceClient WCFClient;

        private int retryCount = 3;
        private readonly TimeSpan delay = TimeSpan.FromSeconds(3);

        /// <summary>
        /// Constructor to instantiate the WCF client.
        /// </summary>
        /// <param name="WCFClient">The WCF client.</param>
        public SkillsRepository(SkillServiceClient WCFClient)
        {
            this.WCFClient = WCFClient;
        }



        /// <summary>
        /// Asynchronous method with retry procedure for the TransientGetSkills task.
        /// </summary>
        /// <returns>IEnumerable list of DTO skills.</returns>
        public async Task<IEnumerable<SkillsDTO>> RetryGetSkills()
        {
            int currentRetry = 0;

            for (;;)
            {
                try
                {
                    return await TransientGetSkills();
                }
                catch (Exception e)
                {
                    Trace.TraceError("WCFClient Exception");

                    currentRetry++;

                    if (currentRetry > this.retryCount)
                    {
                        throw e;
                    }
                }
                await Task.Delay(delay);
            }
        }



        /// <summary>
        /// Asynchronous method to get a list of skills.
        /// </summary>
        /// <returns>IEnumerable list of DTO skills.</returns>
        public async Task<IEnumerable<SkillsDTO>> TransientGetSkills()
        {
            return await WCFClient.GetAllSkillsAsync();
        }



        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SkillsRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}