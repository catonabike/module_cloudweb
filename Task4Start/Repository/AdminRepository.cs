﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace Task4Start.Repository
{
    public class AdminRepository : IAdminRepository, IDisposable
    {
        private HttpClient client;

        private int retryCount = 3;
        private readonly TimeSpan delay = TimeSpan.FromSeconds(3);

        /// <summary>
        /// Constructor sets up API parameters.
        /// </summary>
        /// <param name="client">The specific API resource.</param>
        public AdminRepository(HttpClient client)
        {
            this.client = client;

            client.BaseAddress = new Uri("http://localhost:65026/api/");

            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");

            client.Timeout = TimeSpan.FromSeconds(5);
        }



        /// <summary>
        /// Asynchronous method with retry procedure for the TransientGetAStaff task.
        /// </summary>
        /// <returns>IEnumerable list of DTO skills.</returns>
        public async Task<HttpResponseMessage> RetryGetAStaff(string code)
        {
            int currentRetry = 0;

            for (; ; )
            {
                try
                {
                    return await TransientGetAStaff(code);
                }
                catch (Exception e)
                {
                    Trace.TraceError("HTTP Not Found Exception");

                    currentRetry++;

                    if (currentRetry > this.retryCount)
                    {
                        throw e;
                    }
                }
                await Task.Delay(delay);
            }
        }

        /// <summary>
        /// Asynchronous method to get a member of staffs details.
        /// </summary>
        /// <param name="code">The staffCode.</param>
        /// <returns>A HTTP response.</returns>
        public async Task<HttpResponseMessage> TransientGetAStaff(string code)
        {
            var response = await client.GetAsync("Staff/" + code);

            if (response.IsSuccessStatusCode)
            {
                return response;
            }
            throw new Exception("Resource Not Found");
        }



        /// <summary>
        /// Asynchronous method with retry procedure for the TransientGetBu task.
        /// </summary>
        /// <returns>IEnumerable list of DTO skills.</returns>
        public async Task<HttpResponseMessage> RetryGetBu()
        {
            int currentRetry = 0;

            for (; ; )
            {
                try
                {
                    return await TransientGetBu();
                }
                catch (Exception e)
                {
                    Trace.TraceError("HTTP Not Found Exception");

                    currentRetry++;

                    if (currentRetry > this.retryCount)
                    {
                        throw e;
                    }
                }
                await Task.Delay(delay);
            }
        }

        /// <summary>
        /// Asynchronous method to get a list of business units.
        /// </summary>
        /// <returns>A HTTP response.</returns>
        public async Task<HttpResponseMessage> TransientGetBu()
        {
            var response = await client.GetAsync("BusinessUnit");

            if (response.IsSuccessStatusCode)
            {
                return response;
            }
            throw new Exception("Resource Not Found");
        }



        /// <summary>
        /// Asynchronous method with retry procedure for the TransientGetAStaff task.
        /// </summary>
        /// <returns>IEnumerable list of DTO skills.</returns>
        public async Task<HttpResponseMessage> RetryGetBuStaff(string code)
        {
            int currentRetry = 0;

            for (; ; )
            {
                try
                {
                    return await TransientGetBuStaff(code);
                }
                catch (Exception e)
                {
                    Trace.TraceError("HTTP Not Found Exception");

                    currentRetry++;

                    if (currentRetry > this.retryCount)
                    {
                        throw e;
                    }
                }
                await Task.Delay(delay);
            }
        }

        /// <summary>
        /// Asynchronous method to get a list of staff associated with a business unit.
        /// </summary>
        /// <param name="code">The businessUnitCode.</param>
        /// <returns>A HTTP response.</returns>
        public async Task<HttpResponseMessage> TransientGetBuStaff(string code)
        {
            var response = await client.GetAsync("Staff?code=" + code);

            if (response.IsSuccessStatusCode)
            {
                return response;
            }
            throw new Exception("Resource Not Found");
        }



        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~AdminRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}