﻿using SkillsCoDbfModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Task4Start.Repository
{
    public interface IDataRepository : IDisposable
    {
        // Available interface methods for database requests.
        Task<List<Skill>> GetStaffSkills();
        Task<List<Skill>> GetAStaffsSkills(string code);
        void SaveModel(Skill model);
    }
}