﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Task4Start.Models
{
    public class StaffVM
    {
        public IEnumerable<StaffVM> stUnits;

        [Display(Name = "Code")]
        public string staffCode { get; set; }

        [Display(Name = "Name")]
        public string fullName { get; set; }

        public string businessUnitCode { get; set; }


        /// <summary>
        /// An Asynchronous method to build a list of staff unit models.
        /// </summary>
        /// <param name="response">A JSON API request response.</param>
        /// <param name="id">The staffCode of the member of staff.</param>
        /// <returns>A view model.</returns>
        public static async Task<StaffVM> buildList(HttpResponseMessage response, string id)
        {
            var buStaff = await response.Content.ReadAsAsync<IEnumerable<StaffVM>>();

            var buStMod = new StaffVM()
            {
                businessUnitCode = id,
                stUnits = buStaff.Select(s => new StaffVM()
                {
                    staffCode = s.staffCode.Trim(),
                    fullName = s.fullName.Trim(),
                }).AsEnumerable()
            };
            return buStMod;
        }

    }
}