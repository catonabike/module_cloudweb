﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Task4Start.Models
{
    public class BusinessUnitVM
    {
        [Display(Name = "Code")]
        public string businessUnitCode { get; set; }

        [Display(Name = "Title")]
        public string title { get; set; }


        /// <summary>
        /// An Asynchronous method to build a list of business unit models.
        /// </summary>
        /// <param name="response">A JSON API request response.</param>
        /// <returns>An IEnumerable list of view models.</returns>
        public static async Task<IEnumerable<BusinessUnitVM>> buildList(HttpResponseMessage response)
        {
            var allBu = await response.Content.ReadAsAsync<IEnumerable<BusinessUnitVM>>();

            var busUnits = allBu.Select(b =>
                       new BusinessUnitVM()
                       {
                           businessUnitCode = b.businessUnitCode.Trim(),
                           title = b.title.Trim(),
                       }).AsEnumerable();

            return busUnits;
        }

    }
}