﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using Task4Start.SkillService;

namespace Task4Start.Models
{
    public class SkillVM
    {
        [Key]
        public int staffSkillId { get; set; }

        public string staffCode { get; set; }

        [Display(Name = "Code")]
        public string skillCode { get; set; }

        [Display(Name = "Description")]
        public string skillDescription { get; set; }

        [Display(Name = "Skills options")]
        public List<SelectListItem> SkList { get; set; }

        [Required(ErrorMessage = "&nbsp&nbsp&nbspYou must select a Skill from the list!")]
        public string selectedSkCode { get; set; }

        public string businessUnitCode { get; set; }

        public List<SkillVM> skUnits { get; set; }



        /// <summary>
        /// Method to build a list of skills models.
        /// </summary>
        /// <param name="allSkills">A DTO of skills.</param>
        /// <returns>An IEnumerable list of view models.</returns>
        public static IEnumerable<SkillVM> buildList(IEnumerable<SkillsDTO> allSkills)
        {
            var skUnits = allSkills.Select(s =>
                        new SkillVM()
                        {
                            skillCode = s.skillCode.Trim(),
                            skillDescription = s.skillDescription.Trim(),
                        }).AsEnumerable();

            return skUnits;
        }



        /// <summary>
        /// Method to build a model of a staff members skills.
        /// </summary>
        /// <param name="stSkills">A domain model list of skills.</param>
        /// <param name="id">The staffCode of the member of staff.</param>
        /// <returns>A view model.</returns>
        public static SkillVM buildList(List<SkillsCoDbfModel.Skill> stSkills, string id)
        {
            var stSkUnits = new SkillVM()
            {
                staffCode = id,
                skUnits = stSkills.Select(s => new SkillVM()
                {
                    skillCode = s.skillCode.Trim()
                }).ToList()
            };
            return stSkUnits;
        }



        /// <summary>
        /// Method to build a model with a list of available skills to add to a staff member.
        /// </summary>
        /// <param name="allSkills">A DTO of skills.</param>
        /// <param name="stSkills">A domain model list of skills.</param>
        /// <param name="id">The staffCode of the member of staff.</param>
        public void buildSkList(IEnumerable<SkillsDTO> allSkills, List<SkillsCoDbfModel.Skill> stSkills, string id)
        {
            staffCode = id;

            // Create a new list object.
            List<SelectListItem> list = new List<SelectListItem>();

            // For each skill unit in allSkills.
            foreach (var a in allSkills)
            {
                // Test if any of the skills in the skills database, for this member of staff, match the selected skill code.
                if (stSkills.SingleOrDefault(st => st.skillCode.Trim().Equals(a.skillCode.Trim(), StringComparison.OrdinalIgnoreCase)) != null)
                {
                    // If there is a match, ie the member of staff allready has this skill, then do nothing.
                }
                else
                {
                    // Create new item for the list.
                    SelectListItem item = new SelectListItem
                    {
                        Text = a.skillDescription,
                        Value = a.skillCode,
                    };
                    // Add to the list.
                    list.Add(item);
                }
            }
            // Set reference variable SkList the same as this newly created list.
            this.SkList = list;
        }



        /// <summary>
        /// Method to build an entity framework model to add a new staffSkill to the database.
        /// </summary>
        /// <param name="skillObj">A view model containing the details of the new staffSkill.</param>
        /// <returns>A Domain model to add to the database.</returns>
        public static SkillsCoDbfModel.Skill buildModel(SkillVM skillObj)
        {
            SkillsCoDbfModel.Skill Sk = new SkillsCoDbfModel.Skill();
            
            Sk.staffCode = skillObj.staffCode;
            Sk.skillCode = skillObj.selectedSkCode;
            Sk.isValid = true;

            if (Sk.isValid)
            {
                return Sk;
            }
            throw new Exception("Data Invalid");
        }



        /// <summary>
        /// An Asynchronous method to build a model for a business units available skills.
        /// </summary>
        /// <param name="allSkills">A DTO of skills.</param>
        /// <param name="response">A JSON API request response.</param>
        /// <param name="allStSkills">A DTO of skills.</param>
        /// <param name="id">The staffCode of the member of staff.</param>
        /// <returns>A view model.</returns>
        public static async Task<SkillVM> buildList(IEnumerable<SkillsDTO> allSkills, HttpResponseMessage response, List<SkillsCoDbfModel.Skill> allStSkills, string id)
        {
            var buStaff = await response.Content.ReadAsAsync<IEnumerable<StaffVM>>();

            var buSkMod = new SkillVM()
            {
                businessUnitCode = id,
                skUnits = new List<SkillVM>()
            };

            foreach (var s in allStSkills)
            {
                // Test if the staffSkill database entry belongs to a member of staff who is in the selected business unit AND the skillCode is still valid.
                if (buStaff.SingleOrDefault(b => b.staffCode.Trim().Equals(s.staffCode.Trim(), StringComparison.OrdinalIgnoreCase)) != null &&
                    allSkills.SingleOrDefault(a => a.skillCode.Trim().Equals(s.skillCode.Trim(), StringComparison.OrdinalIgnoreCase)) != null)
                {
                    // If the staff code for the entry matches then add that staffSkill to the list.
                    var item = new SkillVM()
                    {
                        skillCode = s.skillCode
                    };
                    buSkMod.skUnits.Add(item);
                }
            }
            return buSkMod;
        }

    }
}