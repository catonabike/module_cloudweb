namespace SkillsCoDbfModel
{
    using System.Data.Entity;

    /// <summary>
    /// Data Context class for the group of databases used by Task 4.
    /// In effect is a repository pattern for accessing multiple databases.
    /// </summary>
    public partial class StaffSkillsModel : DbContext
    {
        // Specifies the class/model name.
        public StaffSkillsModel() : base("name=StaffSkillsModel")
        {
        }

        // Specifies the class/model name.
        public virtual DbSet<Skill> Skills { get; set; }

        // Sets criteria for Entity Framework when building the Entity Framework model.
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Nothing in this instance!
        }
    }
}
