namespace SkillsCoDbfModel
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Data Context class for the Skill table.
    /// Sets all of the reference variables for use when fetching from or sending data to the database.
    /// </summary>
    public partial class Skill
    {
        [Key] // Annotation specifying that this is the primary key of the entity and it should be unique.
        public int staffSkillId { get; set; }

        [Required]
        [StringLength(50)]
        public string staffCode { get; set; }

        [Required]
        [StringLength(50)]
        public string skillCode { get; set; }

        public bool isValid { get; set; }
    }
}
