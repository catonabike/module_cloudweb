﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HebbraCoDbfModel;
using Task2Start.Models;

namespace Task2Start.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")] // Cross Origin Resource Sharing attribute. Coverts api request to a MVC controller route.
    public class BusinessUnitController : ApiController
    {
        // Creates a new database reference object.
        private HebbraCoDbfModel.HebbraCo16Model context = new HebbraCo16Model();

        /// <summary>
        /// Controller action to return a DTO of all available business units.
        /// </summary>
        /// <returns>A Data Transfer Object to send with the reply to the API request.</returns>
        public IEnumerable<BusinessUnitDTO> GetBu()
        {
            // Get the list of active business units from the database.
            var allBu = context.BusinessUnits.Where(b => b.Active == true);
            // Build the Data Transfer Object.
            var dto = BusinessUnitDTO.buildList(allBu);
            // & return it.
            return dto;
        }
    }
}
