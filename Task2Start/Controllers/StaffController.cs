﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HebbraCoDbfModel;
using Task2Start.Models;

namespace Task2Start.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")] // Cross Origin Resource Sharing attribute. Coverts api request to a MVC controller route.
    public class StaffController : ApiController
    {
        // Creates a new object of the database instance
        private HebbraCoDbfModel.HebbraCo16Model context = new HebbraCo16Model();

        /// <summary>
        /// Controller action to return a DTO of staff associated with a business unit.
        /// </summary>
        /// <param name="code">Takes the business code of the required business unit</param>
        /// <returns>A Data Transfer Object to send with the reply to the API request.</returns>
        public IEnumerable<StaffDTO> GetBuStaff(string code)
        {
            // Creates an object representation of the Business Units database entry selected by the api request.
            var selectBu = context.BusinessUnits.SingleOrDefault(b => b.businessUnitCode.Equals(code, StringComparison.OrdinalIgnoreCase));
            // Creates an object containing all of the staff assigned to the selected business unit.
            var allSt = context.Staffs.Where(b => b.Active == true && b.businessUnitId == selectBu.businessUnitId);
            // Reshapes the object into a Data Transfer Object list to send to the model.
            var dto = StaffDTO.buildList(allSt);
            // Sends the DTO object to the model.
            return dto;
        }


        /// <summary>
        /// Controller action to return a DTO of a staff members details.
        /// </summary>
        /// <param name="id">Takes the staff code of the required staff member.</param>
        /// <returns>A Data Transfer Object to send with the reply to the API request.</returns>
        public StaffDetailDTO GetStaff(string id)
        {
            // Creates an object representation of the member of staffs database entry selected by the api request.
            var thisSt = context.Staffs.SingleOrDefault(st => st.staffCode.Equals(id, StringComparison.OrdinalIgnoreCase));
            // Creates an object containing the Business Unit entry the user is assigned to.
            var thisBu = context.BusinessUnits.SingleOrDefault(b => b.businessUnitId == thisSt.businessUnitId);
            // Reshapes both objects into a Data Transfer Object list to send to the model.
            var dto = StaffDetailDTO.buildStaff(thisBu, thisSt);
            // Sends the DTO object to the model.
            return dto;
        }
    }
}
