﻿using System.Collections.Generic;
using System.Linq;
using HebbraCoDbfModel;

namespace Task2Start.Models
{
    public class BusinessUnitDTO
    {
        public string businessUnitCode { get; set; }

        public string title { get; set; }



        /// <summary>
        /// Method to build a DTO list of available business units.
        /// </summary>
        /// <param name="Allbu">A list of all active business units.</param>
        /// <returns>DTO of active business units.</returns>
        public static IEnumerable<BusinessUnitDTO> buildList(IEnumerable<BusinessUnit> Allbu)
        {
            var busUnits = Allbu.Select(b =>
                       new Models.BusinessUnitDTO()
                       {
                           businessUnitCode = b.businessUnitCode.Trim(),
                           title = b.title.Trim(),
                       }).AsEnumerable();
            return busUnits;
        }
    }
}