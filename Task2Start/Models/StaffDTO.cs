﻿using System.Collections.Generic;
using System.Linq;
using HebbraCoDbfModel;

namespace Task2Start.Models
{
    public class StaffDTO
    {
        public string staffCode { get; set; }

        public string fullName { get; set; }



        /// <summary>
        /// A method method to build a DTO list of staff members.
        /// </summary>
        /// <param name="buStaff">A list of staff.</param>
        /// <returns>A DTO of the staff members details.</returns>
        public static IEnumerable<StaffDTO> buildList(IEnumerable<Staff> buStaff)
        {
            var stUnits = buStaff.Select(s =>
                        new Models.StaffDTO()
                        {
                            staffCode = s.staffCode.Trim(),
                            fullName = BuildName(s.firstName, s.middleName, s.lastName),
                        }).AsEnumerable();

            return stUnits;
        }

        /// <summary>
        /// Method to build a string of a staff members full name.
        /// </summary>
        /// <param name="f">First name.</param>
        /// <param name="m">Middle name.</param>
        /// <param name="l">Last name.</param>
        /// <returns>A trimmed and correctly spaced string of the full name.</returns>
        public static string BuildName(string f, string m, string l)
        {
            if (m != null)
            {
                return f.Trim() + " " + m.Trim() + " " + l.Trim();
            }
            else
            {
                return f.Trim() + " " + l.Trim();
            }
        }
    }
}