﻿namespace Task2Start.Models
{
    public class StaffDetailDTO : StaffDTO
    {
        public string businessUnitCode { get; set; }

        public string dob { get; set; }

        public string startDate { get; set; }

        public string profile { get; set; }

        public string emailAddress { get; set; }

        public string photoUrl { get; set; }



        /// <summary>
        /// A method to build a DTO of a staff members details.
        /// </summary>
        /// <param name="thisBu">A business unit entry.</param>
        /// <param name="thisSt">A staff member entry.</param>
        /// <returns>A DTO of the staff members details.</returns>
        public static StaffDetailDTO buildStaff(HebbraCoDbfModel.BusinessUnit thisBu, HebbraCoDbfModel.Staff thisSt)
        {
            Models.StaffDetailDTO st = new StaffDetailDTO
            {
                businessUnitCode = thisBu.businessUnitCode,
                staffCode = thisSt.staffCode.Trim(),
                fullName = BuildName(thisSt.firstName, thisSt.middleName, thisSt.lastName),
                dob = thisSt.dob.ToString("dd/MM/yyyy"),
                startDate = thisSt.startDate.ToString("dd/MM/yyyy"),
                profile = thisSt.profile,
                emailAddress = thisSt.emailAddress,
            };
            return st;
        }
    }
}