namespace HebbraCoDbfModel
{
    using System.Data.Entity;

    /// <summary>
    /// Data Context class for the group of databases used by Task 1&2.
    /// In effect is a repository pattern for accessing multiple databases.
    /// </summary>
    public partial class HebbraCo16Model : DbContext
    {
        // Specifies the class/model name.
        public HebbraCo16Model() : base("name=HebbraCo16Model")
        {
        }

        // Specifies a BusinessUnit table within the model.
        public virtual DbSet<BusinessUnit> BusinessUnits { get; set; }
        // Specifies the class/model name.
        public virtual DbSet<Staff> Staffs { get; set; }

        // Sets criteria for Entity Framework when building the Entity Framework model.
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Tells Entity Framework that businessUnitCode must be a set length.
            modelBuilder.Entity<BusinessUnit>()
                .Property(e => e.businessUnitCode)
                .IsFixedLength();

            // Tells Entity Framework that officePostCode must be a set length.
            modelBuilder.Entity<BusinessUnit>()
                .Property(e => e.officePostCode)
                .IsFixedLength();

            modelBuilder.Entity<BusinessUnit>()
                // Tells Entity Framework that the relationship between the BusinessUnit & Staff tables is 1 to Many!
                .HasMany(e => e.Staffs)
                // A Staffs entry must be assigned to a BusinessUnit.
                .WithRequired(e => e.BusinessUnit)
                // & deleting a BusinessUnit will not cascade delete the Staffs assigned to it.
                .WillCascadeOnDelete(false);
        }
    }
}
