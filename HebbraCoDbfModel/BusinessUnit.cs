namespace HebbraCoDbfModel
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Data Context class for the BusinessUnit table.
    /// Sets all of the reference variables for use when fetching from or sending data to the database.
    /// </summary>
    [Table("BusinessUnit")] // Annotation specifies the table that the class is mapped to.
    public partial class BusinessUnit
    {
        /// <summary>
        /// Constructor. Instantiates a new HashSet for staff items ready to be used.
        /// </summary>
        public BusinessUnit()
        {
            Staffs = new HashSet<Staff>();
        }
        
        public int businessUnitId { get; set; }
        
        [Required] // Annotation to set & validate this variable as required when entering data.
        [StringLength(10)] // Annotation to set & validate max string length set at 10 characters.
        public string businessUnitCode { get; set; }
        
        [Required]
        [StringLength(50)]
        public string title { get; set; }
        
        [Required]
        public string description { get; set; }
        
        [Required]
        public string officeAddress1 { get; set; }
        
        [Required]
        public string officeAddresss2 { get; set; }
        
        [Required]
        public string officeAddress3 { get; set; }
        
        [Required]
        [StringLength(10)]
        public string officePostCode { get; set; }
        
        [Required]
        public string officeContact { get; set; }

        [Required]
        [StringLength(50)]
        public string officePhone { get; set; }

        [Required]
        [StringLength(50)]
        public string officeEmail { get; set; }

        public bool? Active { get; set; }

        public virtual ICollection<Staff> Staffs { get; set; }
    }
}
