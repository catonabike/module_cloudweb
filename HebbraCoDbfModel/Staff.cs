namespace HebbraCoDbfModel
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Data Context class for the Staff table.
    /// Sets all of the reference variables for use when fetching from or sending data to the database.
    /// </summary>
    [Table("Staff")] // Annotation specifies the table that the class is mapped to.
    public partial class Staff
    {
        public int staffId { get; set; }

        public int businessUnitId { get; set; }

        [Required]
        [StringLength(50)]
        public string staffCode { get; set; }

        [Required]
        public string firstName { get; set; }

      
        public string middleName { get; set; }

        [Required]
        public string lastName { get; set; }

        [Column(TypeName = "date")] // Annotation to set the specific data type that this variable needs to be when getting or setting data.
        public DateTime dob { get; set; }

        [Column(TypeName = "date")]
        public DateTime startDate { get; set; }

        public string profile { get; set; }

        [Required]
        public string emailAddress { get; set; }

        public string photoUrl { get; set; }

        public bool Active { get; set; }

        public virtual BusinessUnit BusinessUnit { get; set; }
    }
}
