﻿using HebbraCoDbfModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Task1Start.Attributes
{
    /// <summary>
    /// A class to describe a Unique attribute that can be used to validate if the specified variable is unique.
    /// </summary>
    public class UniqueAttribute : ValidationAttribute
    {
        private HebbraCo16Model _db = new HebbraCo16Model();

        /// <summary>
        /// Constructor which sets the validation message.
        /// </summary>
        public UniqueAttribute() : base("{0} is not unique.") { }

        /// <summary>
        /// Method to test if a variable with this form already exists in the referenced database.
        /// </summary>
        /// <param name="value">The variable to validate is unique.</param>
        /// <param name="validationContext">The context of the validation.</param>
        /// <returns>The result of the validation test. Success of fail. With an message in case of a failure.</returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // converts the entered item to a string.
            var valueAsString = value.ToString().Trim();

            // If we are validating against the context of business units......
            if (validationContext.DisplayName.Equals("BU Code", StringComparison.OrdinalIgnoreCase))
            {
                // Use the BusinessUnits table.
                var entries = _db.BusinessUnits.Where(b => b.Active == true);
                
                foreach (var e in entries)
                {
                    // And test if the entered code matches any of the businessUnitCodes in the table.
                    if (e.businessUnitCode.Trim().Equals(valueAsString, StringComparison.OrdinalIgnoreCase))
                    {
                        var errorMessage = FormatErrorMessage(validationContext.DisplayName);
                        // If it does then return a fail, with a message.
                        return new ValidationResult(errorMessage);
                    }
                }
                // Else return a success.
                return ValidationResult.Success;
            }
            else // Else we are validating against the context of staff.
            {
                // USing the Staffs table.
                var entries = _db.Staffs.Where(b => b.Active == true);

                foreach (var e in entries)
                {
                    // Test if the entered code matches any of the staffCodes in the table.
                    if (e.staffCode.Trim().Equals(valueAsString, StringComparison.OrdinalIgnoreCase))
                    {
                        var errorMessage = FormatErrorMessage(validationContext.DisplayName);
                        // If it does then return a fail, with a message.
                        return new ValidationResult(errorMessage);
                    }
                }
                // Else return a success.
                return ValidationResult.Success;
            }
        }
    }
}