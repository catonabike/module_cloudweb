﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HebbraCoDbfModel;
using Task1Start.Models;

namespace Task1Start.Controllers
{
    public class BusinessUnitsController : Controller
    {
        /// <summary>
        /// Instantiates an object representation of the database.
        /// </summary>
        private HebbraCo16Model db = new HebbraCo16Model();



        /// <summary>
        /// Controller action to display a list of active business units.
        /// </summary>
        /// <returns>A view model to display.</returns>
        // GET: BusinessUnits
        public ActionResult Index()
        {
            // Creates an object containing all of the Business Units entries that are set as active from the database.
            var allBu = db.BusinessUnits.Where(b => b.Active == true);
            // Re-shapes the new allBu object into a list to display in the view.
            var viewModel = Models.BusinessUnitVM.buildList(allBu);
            // Returns the viewModel to display by the BusinessUnits -> Index view.
            return View(viewModel);
        }



        /// <summary>
        /// Controller action to display the details of a business unit.
        /// </summary>
        /// <param name="id">The selected business unit.</param>
        /// <returns>A view model to display.</returns>
        // GET: BusinessUnits/Details/5
        public ActionResult Details(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                // If the id parameter is empty or null, return a valid HTTP 400 error.
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Creates an object of the entry from the database where the BusinessUnitCode equals the business unit code requested (NOT case sensitive).
            var thisBu = db.BusinessUnits.SingleOrDefault(bu => bu.businessUnitCode.Equals(id, StringComparison.OrdinalIgnoreCase));

            if (thisBu.Active == false || thisBu == null)
            {
                // If that BusinessUnit is NOT active or is NULL the return a valid HTTP 400 error.
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                // Otherwise re-shape the data for the entry to be displayed in the view.
                var viewModel = BusinessUnitDetailVM.buildViewModel(thisBu);
                // Returns the viewModel to display by the BusinessUnits -> Details view.
                return View(viewModel);
            }
        }



        /// <summary>
        /// Controller action to add a new business unit.
        /// </summary>
        /// <returns>An empty view.</returns>
        // GET: BusinessUnits/Create
        public ActionResult Create()
        {
            return View();
        }



        /// <summary>
        /// Controller action to add a new business unit.
        /// </summary>
        /// <param name="businessUnitVM">A BusinessUnit view model.</param>
        /// <returns>A re-direct action to "Index" if validation passed. Otherwise redisplays the view model.</returns>
        // POST: BusinessUnits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "businessUnitCode,title,description,officeAddress1,officeAddresss2,officeAddress3,officePostCode,officeContact,officePhone,officeEmail")] Task1Start.Models.BusinessUnitDetailVM businessUnitVM)
        {
            // Tests if the data entered by the user meets the requirements.
            if (ModelState.IsValid)
            {
                // Creates a model object and transforms the view model to fit and be sent to the database.
                var model = BusinessUnitDetailVM.buildModel(businessUnitVM);
                // Adds the active flag for the business unit to the model, else the added business unit would be in-active and not be visible.
                model.Active = true;
                // The new business unit data is added to the database.
                db.BusinessUnits.Add(model);
                // The new database entry of for the business unit is saved.
                db.SaveChanges();
                // The user is then redirected back to the Index action of the controller.
                return RedirectToAction("Index");
            }
            // If validation of the information the user enters fails then the businessUnitVM object is sent back to the Create view to display.
            return View(businessUnitVM);
        }



        /// <summary>
        /// Controller action to get a business unit entity for editting.
        /// </summary>
        /// <param name="id">The selected business unit.</param>
        /// <returns>A view model to display.</returns>
        // GET: BusinessUnits/Edit/5
        public ActionResult Edit(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                // If the id parameter is empty or NULL the return a valid HTTP 400 error.
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Gets the item from the database with the id requested and creates on object containing the data and sorts the fields.
            var thisBu = db.BusinessUnits.SingleOrDefault(bu => bu.businessUnitCode.Equals(id, StringComparison.OrdinalIgnoreCase));

            if (thisBu.Active == false || thisBu == null)
            {
                // If the business unit is not active or does not exist returns a HTTP 400 bad request error.
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                // Else transforms the object to a view model so the business unit details can be viewed.
                var viewModel = BusinessUnitDetailVM.buildViewModel(thisBu);
                // Returns the viewModel to display by the BusinessUnits -> Edit view.
                return View(viewModel);
            }
        }



        /// <summary>
        /// Controller action to edit a business unit entity.
        /// </summary>
        /// <param name="id">The selected business unit.</param>
        /// <returns>A re-direct action to "Index" if validation passed. Otherwise redisplays the view model.</returns>
        // POST: BusinessUnits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "businessUnitCode,title,description,officeAddress1,officeAddresss2,officeAddress3,officePostCode,officeContact,officePhone,officeEmail")] BusinessUnitDetailVM businessUnitVM)
        {
            // Tests if the data entered by the user meets the requirements set by the HtmlAttributes.
            if (ModelState.IsValid)
            {
                // Gets the business unit entry from the database that matches the edited business unit.
                var efmodel = db.BusinessUnits.SingleOrDefault(bu => bu.businessUnitCode.Equals(businessUnitVM.businessUnitCode, StringComparison.OrdinalIgnoreCase));
                // Creates a model object of the business unit detail and transforms the view model containing the updated data to fit.
                var model = BusinessUnitDetailVM.buildModel(businessUnitVM, efmodel);
                // Sends the edited business unit model to the database and mark the entry as modified.
                db.Entry(model).State = EntityState.Modified;
                // Save the changes to the database.
                db.SaveChanges();
                // The user is then redirected back to the Index action of the controller.
                return RedirectToAction("Index");
            }
            // If validation of the information the user enters fails then the businessUnitVM object is sent back to the Edit view to display.
            return View(businessUnitVM);
        }



        /// <summary>
        /// Controller action to get a business unit for deletion.
        /// </summary>
        /// <param name="id">The selected business unit.</param>
        /// <returns>A view model to display.</returns>
        // GET: BusinessUnits/Delete/5
        public ActionResult Delete(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                // If the id parameter is empty or NULL the return a valid HTTP 400 error.
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Gets the item from the database with the id requested and creates on object containing the data and sorts the fields.
            var thisBu = db.BusinessUnits.SingleOrDefault(bu => bu.businessUnitCode.Equals(id, StringComparison.OrdinalIgnoreCase));

            if (thisBu.Active == false || thisBu == null)
            {
                // If the business unit is not active or does not exist returns a HTTP 400 bad request error.
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest); ;
            }
            else
            {
                // Else transforms the object to a view model so the business unit details can be viewed.
                var viewModel = BusinessUnitDetailVM.buildViewModel(thisBu);
                // Returns the viewModel to display by the BusinessUnits -> Delete view.
                return View(viewModel);
            }
        }



        /// <summary>
        /// Controller action to delete a business unit.
        /// </summary>
        /// <param name="id">The selected business unit.</param>
        /// <returns>A redirect action to "Index".</returns>
        // POST: BusinessUnits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            // Gets the item from the database with the id requested and creates on object containing the data and sorts the fields.
            var thisBu = db.BusinessUnits.SingleOrDefault(bu => bu.businessUnitCode.Equals(id, StringComparison.OrdinalIgnoreCase));
            // Sets the active flag for the business unit to false (soft delete!).
            thisBu.Active = false;
            // Sends the edited business unit model to the database and mark the entry as modified.
            db.Entry(thisBu).State = EntityState.Modified;
            // Save the changes to the database.
            db.SaveChanges();
            // The user is then redirected back to the Index action of the controller.
            return RedirectToAction("Index");
        }



        /// <summary>
        /// Controller action to display a list of staff associated with a business unit.
        /// </summary>
        /// <param name="id">The selected business unit.</param>
        /// <returns>A view model to display.</returns>
        // GET: BusinessUnits/BuStaffList/5
        public ActionResult StaffList(string id)
        {
            // Creates an object containing the Business Unit entry selected by the user.
            var selectBu = db.BusinessUnits.SingleOrDefault(b => b.businessUnitCode.Equals(id, StringComparison.OrdinalIgnoreCase));
            // Creates an object containing all of the Staff entries that are set as active from the database.
            var buStaff = db.Staffs.Where(s => s.Active == true && s.businessUnitId == selectBu.businessUnitId);
            // Re-shapes the new allStaff object into a list to display in the view.
            var viewModel = Models.StaffVM.buildList(buStaff);
            // Add the selected bu description to Viewbag for use in the page title.
            ViewBag.buRef = selectBu.description;
            // Returns the viewModel to display by the BusinessUnit -> StaffList view.
            return View(viewModel);
        }



        /// <summary>
        /// Method to free up any unmanaged resources and free up memory.
        /// </summary>
        /// <param name="disposing">Takes a boolean flag to release resources.</param>
        protected override void Dispose(bool disposing)
        {
            // If you would like to free up unmanaged objects.
            if (disposing)
            {
                // Dispose of all database objects.
                db.Dispose();
            }
            // Call the base class implementation.
            base.Dispose(disposing);
        }
    }
}
