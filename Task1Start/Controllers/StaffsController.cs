﻿using HebbraCoDbfModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Task1Start.Models;

namespace Task1Start.Controllers
{
    /// <summary>
    /// HTTP requests to the BusinessUnits page are routed to the BusinessUnitsController.
    /// </summary>
    public class StaffsController : Controller
    {
        /// <summary>
        /// Creates a new object representation of the database.
        /// </summary>
        private HebbraCo16Model db = new HebbraCo16Model();



        /// <summary>
        /// Controller action to display a list of active staff.
        /// </summary>
        /// <returns>A view model to display.</returns>
        // GET: Staffs
        public ActionResult Index()
        {
            // Creates an object containing all of the Staff entries that are set as active from the database.
            var allStaff = db.Staffs.Where(s => s.Active == true);
            // Re-shapes the new allStaff object into a list to display in the view.
            var viewModel = Models.StaffVM.buildList(allStaff);
            // Returns the viewModel to display by the Staff -> Index view.
            return View(viewModel);
        }



        /// <summary>
        /// Controller action to display the details for a member of staff.
        /// </summary>
        /// <param name="id">The selected member of staff.</param>
        /// <returns>A view model to display.</returns>
        // GET: Staffs/Details/5
        public ActionResult Details(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                // If the id parameter is empty or 0/null, return a valid HTTP 400 error.
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Creates an object of the entry from the database where the StaffCode equals the business unit code requested (NOT case sensitive).
            var thisSt = db.Staffs.SingleOrDefault(st => st.staffCode.Equals(id, StringComparison.OrdinalIgnoreCase));

            if (thisSt.Active == false || thisSt == null)
            {
                // If that Staff is NOT active or is NULL the return a valid HTTP 400 error.
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                // Otherwise re-shape the data for the entry to be displayed in the view.
                var viewModel = StaffDetailVM.buildViewModel(thisSt);
                // Returns the viewModel to display by the Staffs -> Details view.
                return View(viewModel);
            }
        }



        /// <summary>
        /// Controller action to add a new staff member.
        /// </summary>
        /// <returns>An empty view.</returns>
        // GET: Staffs/Create
        public ActionResult Create()
        {
            // Creates an object containing all of the Business Units entries that are set as active from the database.
            var allBu = db.BusinessUnits.Where(b => b.Active == true);
            // Instantiates a new StaffDetailVM view model.
            StaffDetailVM viewModel = new StaffDetailVM();
            // Builds the empty view model to display when creating a new member of staff.
            viewModel.buildBuList(allBu, "");
            // Returns the viewModel to display by the Staffs -> Edit view.
            return View(viewModel);
        }



        /// <summary>
        /// Controller action to add a new staff member.
        /// </summary>
        /// <param name="staffVM">A Staff view model.</param>
        /// <returns>A re-direct action to "Index" if validation passed. Otherwise redisplays the view model.</returns>
        // POST: Staffs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "staffCode,firstName,middleName,lastName,selectedBuCode,dob,startDate,profile,emailAddress,photoUrl")] StaffDetailVM staffVM)
        {
            // Tests if the data entered by the user meets the requirements.
            if (ModelState.IsValid)
            {
                // Creates an object containing the Business Unit entry selected by the user.
                var selectBu = db.BusinessUnits.SingleOrDefault(b => b.businessUnitCode.Equals(staffVM.selectedBuCode, StringComparison.OrdinalIgnoreCase));
                // Creates a model object and transforms the view model to fit and be sent to the database.
                var model = StaffDetailVM.buildModel(selectBu, staffVM);
                // The new staff data is added to the database.
                SaveModel(model);
                // The user is then redirected back to the Index action of the controller.
                return RedirectToAction("Index");
            }
            // If validation of the information the user enters fails then the staffVM object is sent back to the Create view to display.
            return View(staffVM);
        }



        /// <summary>
        /// Method to add & save new entries to a database.
        /// </summary>
        /// <param name="model">The Entity Framework model to add.</param>
        private void SaveModel(Staff model)
        {
            // The new  newly created model is added to the database.
            db.Staffs.Add(model);
            // The new database entry of for the staff is saved.
            db.SaveChanges();
        }



        /// <summary>
        /// Controller action to get a staff member to edit.
        /// </summary>
        /// <param name="id">The selected member of staff.</param>
        /// <returns>A view model to display.</returns>
        // GET: Staffs/Edit/5
        public ActionResult Edit(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                // If the id parameter is empty or NULL the return a valid HTTP 400 error.
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Gets the item from the database with the id requested and creates on object containing the data and sorts the fields.
            var thisSt = db.Staffs.SingleOrDefault(st => st.staffCode.Equals(id, StringComparison.OrdinalIgnoreCase));
            // Creates an object containing all of the Business Units entries that are set as active from the database.
            var allBu = db.BusinessUnits.Where(b => b.Active == true);

            if (thisSt.Active == false || thisSt == null)
            {
                // If the staff is not active or does not exist returns a HTTP 400 bad request error.
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                // Instantiates a new StaffDetailVM view model.
                StaffDetailVM viewModel = new StaffDetailVM();
                // Else transforms the object to a view model so the staff details can be viewed.
                viewModel = StaffDetailVM.buildViewModel(thisSt);
                // And builds/adds a list of available business units to the model for selection.
                viewModel.buildBuList(allBu, thisSt.BusinessUnit.businessUnitCode);
                // Returns the viewModel to display by the Staffs -> Edit view.
                return View(viewModel);
            }
        }



        /// <summary>
        /// Controller action to edit a staff member.
        /// </summary>
        /// <param name="staffVM">The editted staff member model.</param>
        /// <returns>A redirect to the "Index" action if validation is successful. Otherwise a redirect to "Index".</returns>
        // POST: Staffs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "staffCode,firstName,middleName,lastName,selectedBuCode,dob,startDate,profile,emailAddress,photoUrl")] StaffDetailVM staffVM)
        {
            // Tests if the data entered by the user meets the requirements.
            if (ModelState.IsValid)
            {
                // Creates an object containing the Business Unit entry selected by the user.
                var selectBu = db.BusinessUnits.SingleOrDefault(b => b.businessUnitCode.Equals(staffVM.selectedBuCode, StringComparison.OrdinalIgnoreCase));
                // Gets the staff entry from the database that matches the edited staff entry.
                var efmodel = db.Staffs.SingleOrDefault(st => st.staffCode.Equals(staffVM.staffCode, StringComparison.OrdinalIgnoreCase));
                // Creates a model object of the staff entries detail and transforms the view model containing the updated data to fit.
                var model = StaffDetailVM.buildModel(staffVM, selectBu, efmodel);
                // Sends the edited staff model to the database and mark the entry as modified.
                db.Entry(model).State = EntityState.Modified;
                // Save the changes to the database.
                db.SaveChanges();
                // The user is then redirected back to the Index action of the controller.
                return RedirectToAction("Index");
            }
            // If validation of the information the user enters fails then the staffVM object is sent back to the Edit view to display.
            return View(staffVM);
        }



        /// <summary>
        /// Controller action to get the staff member to delete.
        /// </summary>
        /// <param name="id">The selected member of staff.</param>
        /// <returns>A view model to display.</returns>
        // GET: Staffs/Delete/5
        public ActionResult Delete(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                // If the id parameter is empty or NULL the return a valid HTTP 400 error.
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Gets the item from the database with the id requested and creates on object containing the data and sorts the fields.
            var thisSt = db.Staffs.SingleOrDefault(st => st.staffCode.Equals(id, StringComparison.OrdinalIgnoreCase));

            if (thisSt.Active == false || thisSt == null)
            {
                // If the staff entry is not active or does not exist returns a HTTP 400 bad request error.
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest); ;
            }
            else
            {
                // Else transforms the object to a view model so the business unit details can be viewed.
                var viewModel = StaffDetailVM.buildViewModel(thisSt);
                // Returns the viewModel to display by the Staffs -> Delete view.
                return View(viewModel);
            }
        }



        /// <summary>
        /// Controller action to delete a member of staff.
        /// </summary>
        /// <param name="id">The seleceted member of staff.</param>
        /// <returns>A re-direct to the Index action of the Staffs controller.</returns>
        // POST: Staffs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            // Gets the item from the database with the id requested and creates on object containing the data and sorts the fields.
            var thisSt = db.Staffs.SingleOrDefault(st => st.staffCode.Equals(id, StringComparison.OrdinalIgnoreCase));
            // Sets the active flag for the staff entry to false (soft delete!).
            thisSt.Active = false;
            // Sends the edited staff model to the database and mark the entry as modified.
            db.Entry(thisSt).State = EntityState.Modified;
            // Save the changes to the database.
            db.SaveChanges();
            // The user is then redirected back to the Index action of the controller.
            return RedirectToAction("Index");
        }



        /// <summary>
        /// Method to free up any unmanaged resources and free up memory.
        /// </summary>
        /// <param name="disposing">Takes a boolean flag to release resources.</param>
        protected override void Dispose(bool disposing)
        {
            // If you would like to free up unmanaged objects.
            if (disposing)
            {
                // Dispose of all database objects.
                db.Dispose();
            }
            // Call the base class implementation.
            base.Dispose(disposing);
        }
    }
}

