﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task1Start.Controllers
{
    /// <summary>
    /// HTTP requests to the Home page are routed to this HomeController.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Method responsible for routing requests using the Index action of the URL.
        /// If no option is given then the user will also be routed to the Index action.
        /// </summary>
        /// <returns>an empty View to send to the Home -> Index view.</returns>
        public ActionResult Index()
        {
            return View();
        }



        /// <summary>
        /// Method responsible for routing requests to the About action of the URL.
        /// </summary>
        /// <returns>an empty View to send to the Home -> About view.</returns>
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }



        /// <summary>
        /// Method responsible for routing requests to the Contact action of the URL.
        /// </summary>
        /// <returns>an empty View to send to the Home -> Contact view.</returns>
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
    }
}