﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task1Start.Models
{
    public class BusinessUnitDetailVM : BusinessUnitVM
    {
        /// <summary>
        /// Defines a variable for description.
        /// </summary>
        [Required]
        [Display(Name = "Brief Description")]
        public string description { get; set; }

        /// <summary>
        /// Defines a variable for officeAddress1.
        /// </summary>
        [Required]
        [Display(Name = "Address Line 1")]
        public string officeAddress1 { get; set; }

        /// <summary>
        /// Defines a variable for officeAddress2.
        /// </summary>
        [Required]
        [Display(Name = "Address Line 2")]
        public string officeAddresss2 { get; set; }

        /// <summary>
        /// Defines a variable for officeAddress3.
        /// </summary>
        [Required]
        [Display(Name = "Address Line 3")]
        public string officeAddress3 { get; set; }

        /// <summary>
        /// Defines a variable for officePostCode.
        /// </summary>
        [Required]
        [StringLength(10)]
        [Display(Name = "Post Code")]
        [DataType(DataType.PostalCode)]
        public string officePostCode { get; set; }

        /// <summary>
        /// Defines a variable for officeContact.
        /// </summary>
        [Required]
        [Display(Name = "Main Contact Full Name")]
        public string officeContact { get; set; }

        /// <summary>
        /// Defines a variable for officePhone.
        /// </summary>
        [Required]
        [StringLength(50)]
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        public string officePhone { get; set; }

        /// <summary>
        /// Defines a variable for officeEmail.
        /// </summary>
        [Required]
        [StringLength(50)]
        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string officeEmail { get; set; }

        /// <summary>
        /// Defines a hidden variable called message.
        /// </summary>
        [HiddenInput(DisplayValue = false)]
        public string message { get; set; }



        /// <summary>
        /// Method to re-shape a business unit details Domain Model into a View Model to be be displayed by a View.
        /// </summary>
        /// <param name="thisBu">Takes a business unit details Domain Model.</param>
        /// <returns>A ViewModel to be sent to the view engine for routing to a View.</returns>
        public static Models.BusinessUnitDetailVM buildViewModel(HebbraCoDbfModel.BusinessUnit thisBu)
        {
            // A new view model object is created.
            BusinessUnitDetailVM vm = new BusinessUnitDetailVM
            {
                // And the required variables are set from the domain model passed in.
                businessUnitCode = thisBu.businessUnitCode.Trim(),
                title = thisBu.title,
                description = thisBu.description,
                officeAddress1 = thisBu.officeAddress1,
                officeAddresss2 = thisBu.officeAddresss2,
                officeAddress3 = thisBu.officeAddress3,
                officeContact = thisBu.officeContact,
                officeEmail = thisBu.officeEmail,
                officePhone = thisBu.officePhone,
                officePostCode = thisBu.officePostCode
            };
            return vm;
        }



        /// <summary>
        /// Method used to create a new business unit details Domain Model for a new business unit from the details entered into the View Model by the user.
        /// </summary>
        /// <param name="businessUnit">Takes a business unit details View Model.</param>
        /// <returns>A DomainModel that can be sent to the database.</returns>
        public static HebbraCoDbfModel.BusinessUnit buildModel(BusinessUnitDetailVM businessUnit)
        {
            // A new domain model that can be added to the database is created.
            HebbraCoDbfModel.BusinessUnit Bu = new HebbraCoDbfModel.BusinessUnit();
            // And the data fields are added to the model.
            Bu.businessUnitCode = businessUnit.businessUnitCode;
            Bu.title = businessUnit.title;
            Bu.description = businessUnit.description;
            Bu.officeAddress1 = businessUnit.officeAddress1;
            Bu.officeAddresss2 = businessUnit.officeAddresss2;
            Bu.officeAddress3 = businessUnit.officeAddress3;
            Bu.officeContact = businessUnit.officeContact;
            Bu.officeEmail = businessUnit.officeEmail;
            Bu.officePhone = businessUnit.officePhone;
            Bu.officePostCode = businessUnit.officePostCode;
            Bu.Active = true; // The newly created business unit is set as active.
            return Bu;
        }



        /// <summary>
        /// Method used to re-shape a view model into a Domain Model to update details in the database.
        /// </summary>
        /// <param name="businessUnit">The View Model with the new/updated data.</param>
        /// <param name="Bu">An oblect of the original Domain Model of the business unit to compare the fields to and track changes.</param>
        /// <returns>A Domain Model that can be sent to the database.</returns>
        public static HebbraCoDbfModel.BusinessUnit buildModel(BusinessUnitDetailVM businessUnit, HebbraCoDbfModel.BusinessUnit Bu)
        {
            // The data fields are transfered from the view model to the domain model passed in.
            Bu.businessUnitCode = businessUnit.businessUnitCode;
            Bu.title = businessUnit.title;
            Bu.description = businessUnit.description;
            Bu.officeAddress1 = businessUnit.officeAddress1;
            Bu.officeAddresss2 = businessUnit.officeAddresss2;
            Bu.officeAddress3 = businessUnit.officeAddress3;
            Bu.officeContact = businessUnit.officeContact;
            Bu.officeEmail = businessUnit.officeEmail;
            Bu.officePhone = businessUnit.officePhone;
            Bu.officePostCode = businessUnit.officePostCode;
            Bu.Active = true; // The newly created business unit is set as active.
            return Bu;
        }
    }
}