﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task1Start.Models
{
    public class StaffDetailVM : StaffVM
    {
        /// <summary>
        /// Defines a List object BuList.
        /// </summary>
        [Display(Name = "Select Business Unit")]
        public List<SelectListItem> BuList { get; set; }

        /// <summary>
        /// Defines a variable for selectedBuCode.
        /// </summary>
        public string selectedBuCode { get; set; }

        /// <summary>
        /// Defines a variable for dob.
        /// </summary>
        [Display(Name = "Date of Birth")]
        public string dob { get; set; }

        /// <summary>
        /// Defines a variable for startDate.
        /// </summary>
        [Display(Name = "Start Date")]
        public string startDate { get; set; }

        /// <summary>
        /// Defines a variable for profile.
        /// </summary>
        [Display(Name = "Profile")]
        public string profile { get; set; }

        /// <summary>
        /// Defines a variable for emailAddress.
        /// </summary>
        [Required]
        [Display(Name = "e-mail")]
        public string emailAddress { get; set; }

        /// <summary>
        /// Defines a variable for photoUrl.
        /// </summary>
        [Display(Name = "Photo URL")]
        public string photoUrl { get; set; }



        /// <summary>
        /// Method to re-shape a staff details Domain Model into a View Model to be be displayed by a View.
        /// </summary>
        /// <param name="thisSt">Takes a staff details Domain Model.</param>
        /// <returns>A View Model to be sent to the view engine for routing.</returns>
        public static Models.StaffDetailVM buildViewModel(HebbraCoDbfModel.Staff thisSt)
        {
            // A new view model object is created.
            StaffDetailVM vm = new StaffDetailVM
            {
                // And the required variables are set from the domain model passed in.
                fullName = BuildName(thisSt.firstName, thisSt.middleName, thisSt.lastName),
                staffCode = thisSt.staffCode.Trim(),
                firstName = thisSt.firstName,
                middleName = thisSt.middleName,
                lastName = thisSt.lastName,
                dob = thisSt.dob.ToString("dd/MM/yyyy"),
                startDate = thisSt.startDate.ToString("dd/MM/yyyy"),
                profile = thisSt.profile,
                emailAddress = thisSt.emailAddress,
                photoUrl = thisSt.photoUrl,
            };
            return vm;
        }



        /// <summary>
        /// Method used to create a staff unit details Domain Model for a new staff member from the details entered into the View Model by the user.
        /// </summary>
        /// <param name="staffObj">Takes a staff details View Model.</param>
        /// <returns>A DomainModel that can be sent to the database.</returns>
        public static HebbraCoDbfModel.Staff buildModel(HebbraCoDbfModel.BusinessUnit bu, StaffDetailVM staffObj)
        {
            // A new domain model that can be added to the database is created.
            HebbraCoDbfModel.Staff St = new HebbraCoDbfModel.Staff();
            // And the data fields are added to the model.
            St.businessUnitId = bu.businessUnitId;
            St.staffCode = staffObj.staffCode;
            St.firstName = staffObj.firstName;
            St.middleName = staffObj.middleName;
            St.lastName = staffObj.lastName;
            St.dob = DateTime.Parse(staffObj.dob);
            St.startDate = DateTime.Parse(staffObj.startDate);
            St.profile = staffObj.profile;
            St.emailAddress = staffObj.emailAddress;
            St.photoUrl = staffObj.photoUrl;
            St.Active = true; // The newly created staff unit is set as active.
            return St;
        }



        /// <summary>
        /// Method used to re-shape a view model into a Domain Model to add or update details from the Edit action in the database.
        /// </summary>
        /// <param name="staffObj">The View Model with the new/updated data.</param>
        /// <param name="St">An oblect of the original Domain Model of the business unit to compare the fields to and track changes.</param>
        /// <returns>A Domain Model that can be sent to the database.</returns>
        public static HebbraCoDbfModel.Staff buildModel(StaffDetailVM staffObj, HebbraCoDbfModel.BusinessUnit bu, HebbraCoDbfModel.Staff St)
        {
            // The data fields are transfered from the view model to the domain model passed in.
            St.businessUnitId = bu.businessUnitId;
            St.staffCode = staffObj.staffCode;
            St.firstName = staffObj.firstName;
            St.middleName = staffObj.middleName;
            St.lastName = staffObj.lastName;
            St.dob = DateTime.Parse(staffObj.dob);
            St.startDate = DateTime.Parse(staffObj.startDate);
            St.profile = staffObj.profile;
            St.emailAddress = staffObj.emailAddress;
            St.photoUrl = staffObj.photoUrl;
            St.Active = true; // The newly created staff unit is set as active.
            return St;
        }



        /// <summary>
        /// Method to build a list of business unit codes.
        /// </summary>
        /// <param name="bu">On object containing all active business units.</param>
        /// <param name="id">The businessUnitCode currently selected for the member of staff selected.</param>
        public void buildBuList(IEnumerable<HebbraCoDbfModel.BusinessUnit> bu, string id)
        {
            // Create a new list object.
            List<SelectListItem> list = new List<SelectListItem>();

            // For each business unit in bu.
            foreach (var b in bu)
            {
                // Test if this is the currently selected business unit code for the employee.
                if (b.businessUnitCode.Equals(id))
                {
                    // Create new item for the list.
                    SelectListItem item = new SelectListItem
                    {
                        Text = b.businessUnitCode,
                        Value = b.businessUnitCode,
                        // Mark this list item as the currently selected code.
                        Selected = true
                    };
                    // Add to the list.
                    list.Add(item);
                }
                else
                {
                    // Create new item for the list.
                    SelectListItem item = new SelectListItem
                    {
                        Text = b.businessUnitCode,
                        Value = b.businessUnitCode
                    };
                    // Add to the list.
                    list.Add(item);
                }
            }
            // Set reference variable BuList the same as this newly created list.
            this.BuList = list;
        }
    }
}