﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Task1Start.Attributes;

namespace Task1Start.Models
{
    public class StaffVM
    {
        /// <summary>
        /// Defines a variable for staffCode.
        /// </summary>
        [Required]
        [StringLength(50)]
        [Unique] // This is my custom built "Unique" Attribute to validate that a duplicate code is not entered.
        [Display(Name = "Code")]
        public string staffCode { get; set; }

        /// <summary>
        /// Defines a variable for firstName.
        /// </summary>
        [Required]
        [Display(Name = "Forname")]
        public string firstName { get; set; }

        /// <summary>
        /// Defines a variable for middleName.
        /// </summary>
        [Display(Name = "Middle Name")]
        public string middleName { get; set; }

        /// <summary>
        /// Defines a variable for lastName.
        /// </summary>
        [Required]
        [Display(Name = "Surname")]
        public string lastName { get; set; }

        /// <summary>
        /// Defines a variable for fullName.
        /// </summary>
        [Display(Name = "Name")]
        public string fullName { get; set; }



        /// <summary>
        /// Method to build a View Model list of all staff to be displayed in the View.
        /// </summary>
        /// <param name="allStaff">Takes a Domain Model list of available staff.</param>
        /// <returns>A View Model to be sent to the view engine for routing to a view.</returns>
        public static IEnumerable<Models.StaffVM> buildList(IEnumerable<HebbraCoDbfModel.Staff> allStaff)
        {
            var stUnits = allStaff.Select(s =>
                        new Models.StaffVM()
                        {
                            staffCode = s.staffCode.Trim(),
                            fullName = BuildName(s.firstName, s.middleName, s.lastName),
                        }).AsEnumerable(); // For each database entry, a new BusinessUnitVM is created and added to the IEnumerable list.

            return stUnits;
        }



        /// <summary>
        /// Method to build a string of a staff members full name.
        /// </summary>
        /// <param name="f">First name.</param>
        /// <param name="m">Middle name.</param>
        /// <param name="l">Last name.</param>
        /// <returns>A trimmed and correctly spaced string of the full name.</returns>
        public static string BuildName(string f, string m, string l)
        {
            // Test if there is a middle name entered for the entry.
            if (m != null)
            {
                return f.Trim() + " " + m.Trim() + " " + l.Trim();
            }
            else
            {
                return f.Trim() + " " + l.Trim();
            }
        }
    }
}