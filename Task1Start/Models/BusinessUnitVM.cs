﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Task1Start.Attributes;

namespace Task1Start.Models
{
    public class BusinessUnitVM
    {
        /// <summary>
        /// Defines a variable for businessUnitCode.
        /// </summary>
        [Required]
        [StringLength(10)]
        [Unique] // This is my custom built "Unique" Attribute to validate that a duplicate code is not entered.
        [Display(Name = "BU Code")]
        public string businessUnitCode { get; set; }

        /// <summary>
        /// Defines a variable for title.
        /// </summary>
        [Required]
        [StringLength(50)]
        [Display(Name = "Unit Name")]
        public string title { get; set; }


        /// <summary>
        /// Method to build a View Model list of all business units to be displayed in the View.
        /// </summary>
        /// <param name="allBu">Takes a Domain Model list of available business units.</param>
        /// <returns>A View Model to be sent to the view engine for routing to a view.</returns>
        public static IEnumerable<Models.BusinessUnitVM> buildList(IEnumerable<HebbraCoDbfModel.BusinessUnit> allBu)
        {
            var busUnits = allBu.Select(b =>
                        new Models.BusinessUnitVM()
                        {
                            businessUnitCode = b.businessUnitCode.Trim(),
                            title = b.title,
                        }).AsEnumerable(); // For each database entry, a new BusinessUnitVM is created and added to the IEnumerable list.

            return busUnits;
        }
    }
}